#
# sfw - Common functions
#
# Copyright (C) 2020-2024 (Daniel Huhardeaux devel@tootai.net)
#
# Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence Publique Générale GNU
# publiée par la Free Software Foundation (version 2 ou bien toute autre version ultérieure choisie par vous).
#
# Ce programme est distribué car potentiellement utile, mais SANS AUCUNE GARANTIE, ni explicite ni implicite, y compris les garanties
# de commercialisation ou d'adaptation dans un but spécifique. Reportez-vous à la Licence Publique Générale GNU pour plus de détails.
#
# Vous devez avoir reçu une copie de la Licence Publique Générale GNU en même temps que ce programme;
# si ce n'est pas le cas, écrivez à la Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, États-Unis.
# ou visitez <https://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Create fail2ban tables and chain
#

fail2ban() {
    # from include "/etc/nftables/fail2ban.conf"
    tmp_output=$myoutput/$1/rules

    if [ "$1" = "ip6" ]; then
        echo "add table $1 fail2ban" >>$tmp_output
        echo "add chain $1 fail2ban input { type filter hook input priority 100 ; policy accept ; }" >>$tmp_output || true
        echo "add set $1 fail2ban f2b-asterisk-udp { type ipv6_addr; }" >>$tmp_output
        echo "add set $1 fail2ban f2b-asterisk-tcp { type ipv6_addr; }" >>$tmp_output
    else
        echo "add table $1 fail2ban" >>$tmp_output
        echo "add chain $1 fail2ban input { type filter hook input priority 100 ; policy accept ; }" >>$tmp_output || true
        echo "add set $1 fail2ban f2b-asterisk-udp { type ipv4_addr; }" >>$tmp_output
        echo "add set $1 fail2ban f2b-asterisk-tcp { type ipv4_addr; }" >>$tmp_output
    fi
}

# Reject denied IP or IP%ports
#
denyipports() {
    tmp_output=$myoutput/$1/rules.$IFACE

    for element in $2; do
        [ $debug -gt 0 ] && echo "$myname - DENY We treat element $element"

        myproto=$(echo $element|cut -d \% -f1)
        myiaddr=$(echo $element|cut -d \% -f2|cut -d \( -f1)
        myiport=$(echo $element|cut -d \% -f3)

        [ "x$myiaddr" = "x"                                                 ] && myiaddr=$3
        [ "x$myiport" = "xALL" -o "x$myiport" = "xall" -o "x$myiport" = "x" ] && mydport="0-65534" || mydport=$myiport
        [ "x$myproto" = "xALL" -o "x$myproto" = "xall" -o "x$myproto" = "x" ] && mydport=""

        [ "$debug"    = "1" ] && echo "$myname - DENY proto=$myproto addr=$myiaddr port=$mydport"
        [ "x$mydport" = "x" ] && echo "add element $1 nat blocklist { $myiaddr }" >>$tmp_output \
                              || echo "add element $1 nat blocklist_$myproto.$IFACE { $myiaddr . $mydport : drop }" >>$tmp_output
    done
}

# Forwarding
#
forward() {
  tmp_output=$myoutput/$1/rules.$IFACE

  for element in $2; do
    [ $debug -gt 0 ] && echo "$myname - FORWARD We treat element $element"

    # We remove Srcnat or Masq demand if any
    # 
    mysnat=$(echo $element|grep -o ".$"|tr [:lower:] [:upper:])
    [ "$mysnat" = "S" -o "$mysnat" = "M" ] && element=$(echo $element|sed 's/.$//')

    myproto=$(echo $element|cut -d \% -f1)                                      # current protocol
    myiaddr=$(echo $element|cut -d \% -f2|cut -d \@ -f1|cut -d \( -f1)          # destination ip
    myiport=$(echo $element|cut -d \@ -f2|cut -d \% -f1)                        # destination port(s)
    mydaddr=$(echo $element|cut -d \% -f3|cut -d \@ -f1|cut -d \( -f1)          # forwarding ip if any
    mydport=$(echo $element|cut -d \@ -f3)                                      # forwrding port(s) if any

    [ "x$myiaddr" = "x"                                ] && myiaddr=$3
    [ "x$myiaddr" != "x$3"                             ] && continue            # $3 IP is not concerned by the current rule
    [ "x$mydaddr" = "x"                                ] && mydaddr=$myiaddr
    [ "x$myiport" = "xALL" -o "x$myiport" = "xall"     ] && myiport="1-65534"
    [ "x$mydport" = "x"                                ] && mydport=$myiport
    [ "x$mysnat"  = "x"                                ] && mydport=$myiport

    if [ "$4" = "post" ]; then
      if [ "$myiaddr" != "$mydaddr" ]; then
        if [ "$mysnat" = "S" ]; then
          [ $debug -gt 0 ] && echo "$myname - Set postrouting REDIRECT snat rules to IP $mydaddr"
          newrule="add rule $1 nat postrouting oif $IFACE $1 daddr $mydaddr $myproto dport { $mydport } snat $myiaddr"
          rule_already_exist "$newrule" $tmp_output
          [ $? = 1 ] && echo "$newrule" >>$tmp_output
        else
          if [ "$mysnat" = "M" ]; then
            [ $debug -gt 0 ] && echo "$myname - Set postrouting REDIRECT masquerade rules to IP $mydaddr"
            newrule="add rule $1 nat postrouting oif $IFACE $1 daddr $mydaddr $myproto dport { $mydport } masquerade"
            rule_already_exist "$newrule" $tmp_output
            [ $? = 1 ] && echo "$newrule" >>$tmp_output
          fi
        fi
      fi

      continue
    fi

    [ $debug -gt 0 ] && echo "$myname - FORWARD proto=$myproto addr=$myiaddr port=$myiport TO addr=$mydaddr port=$mydport"

    if [ "$mydport" = "$myiport" ]; then
      if [ "$myiaddr" != "$mydaddr" ]; then
        # in case someone want to test stupid thinks like dnat to same IP and port(s)
        #
        newrule="add rule $1 nat prerouting $1 daddr $myiaddr $myproto dport { $myiport } dnat to $mydaddr"
        rule_already_exist "$newrule" $tmp_output
        [ $? = 1 ] && echo "$newrule" >>$tmp_output

		# dport is a range or unique port, we add it to set of allowed ports if no srcnat or masq setted
		#
		if [ "$mysnat" != "S" -a "$mysnat" != "M" ]; then  
    	  newrule="add element $1 filter redirect_$myproto.$IFACE { $myiport }"
      	  rule_already_exist "$newrule" $tmp_output
      	  [ $? = 1 ] && echo "$newrule" >>$tmp_output
		fi
      fi
    else
      if [ "$myiaddr" != "$mydaddr" ]; then
        tmpsprf=$(echo $myiport |cut -d - -f1)     # first src port in range
        tmpsprl=$(echo $myiport |cut -d - -f2)     # last src port in range
        tmpdprf=$(echo $mydport |cut -d - -f1)     # first dst port in range
        tmpdprl=$(echo $mydport |cut -d - -f2)     # last dst port in range

        for (( j=$tmpsprf; j<=$tmpsprl; j++ )); do
          if [ $nftnumvers -lt 100 ]; then
            newrule="add element $1 nat fwdtoip_$myproto.$IFACE { $j : $mydaddr }"
            rule_already_exist "$newrule" $tmp_output
            [ $? = 1 ] && echo "$newrule" >>$tmp_output

            newrule="add element $1 nat fwdtoport_$myproto.$IFACE { $j : $tmpdprf }"
          else
            newrule="add element $1 nat forward_$myproto.$IFACE { $myiaddr . $j : $mydaddr . $tmpdprf }"
          fi
          rule_already_exist "$newrule" $tmp_output
          [ $? = 1 ] && echo "$newrule" >>$tmp_output

		  # dport is a range or unique port, we add it to set of allowed ports
		  #
      	  newrule="add element $1 filter redirect_$myproto.$IFACE { $tmpdprf }"
          rule_already_exist "$newrule" $tmp_output
          [ $? = 1 ] && echo "$newrule" >>$tmp_output

		  [ "$tmpdprf" -lt "$tmpdprl" ] && tmpdprf=$((tmpdprf+1))
        done

        newrule="add rule $1 nat prerouting dnat to $1 daddr . $myproto dport map @forward_$myproto.$IFACE"
        rule_already_exist "$newrule" $tmp_output
        [ $? = 1 ] && echo "$newrule" >>$tmp_output
      else
        # Redirect like as redirect does not work
        #
		# dport is a range or uniq port, we add it to set of allowed ports
		#
      	newrule="add element $1 filter redirect_$myproto.$IFACE { $mydport }"
      	rule_already_exist "$newrule" $tmp_output
      	[ $? = 1 ] && echo "$newrule" >>$tmp_output

        #newrule="add rule $1 nat prerouting $1 daddr $myiaddr $myproto dport { $myiport } redirect to :$mydport"
        newrule="add rule $1 nat prerouting $1 daddr $myiaddr $myproto dport { $myiport } dnat to :$mydport"
        rule_already_exist "$newrule" $tmp_output
        [ $? = 1 ] && echo "$newrule" >>$tmp_output
      fi
    fi
  done
}

# Apply mangle routing rules
#
mark_rules_mangle_src() {
  # $1 is ip or ip6
  # $2 net/ip to forward
  # $3 routing mark
  # $4 local_net
  #
  newrule="add rule $1 mangle prerouting iif \"$routing_iface\" $1 saddr { $2 } ip6 daddr != @$4 meta mark set $3 ct mark set meta mark"
  rule_already_exist "$newrule" $file_output.0
  [ $? = 1 ] && echo "$newrule" >>$file_output.0
}

mark_rules_mangle_dst() {
  # $1 is ip or ip6
  # $2 net/ip to forward
  # $3 routing mark
  # $4 local_net
  #
  newrule="add rule $1 mangle prerouting iif \"$routing_iface\" $1 daddr { $2 } ip6 daddr != @$4 meta mark set $3 ct mark set meta mark"
  rule_already_exist "$newrule" $file_output.0
  [ $? = 1 ] && echo "$newrule" >>$file_output.0
}

mark_rules_mangle() {
  # $1 is ip or ip6
  # $2 net/ip to forward
  # $3 routing mark
  # $4 local_net
  #
  mark_rules_mangle_src $1 $2 $3 $4
  # 20230816 keeped in case of ...
  #mark_rules_mangle_dst $1 $2 $3 $4
}

# Apply nat routing rules
#
mark_rules_nat() {
  # $1 is ip or ip6
  # $2 ip to forward to
  # $3 routing mark
  #
  [ "$1" = "ip" ] && tmpproto="icmp" || tmpproto="ipv6-icmp"
  newrule="add rule $1 nat prerouting meta mark $3 $1 nexthdr tcp ip6 nexthdr udp meta l4proto $tmpproto dnat to $(echo $2|cut -d \/ -f1)"
  rule_already_exist "$newrule" $file_output.0
  [ $? = 1 ] && echo "$newrule" >>$file_output.0
}

# Apply local filter routing rules
#
mark_rules_filter_local() {
  # $1 is ip or ip6
  #
  newrule="add rule $1 filter input meta mark $local_mark accept" >>$file_output.0
  rule_already_exist "$newrule" $file_output.0
  [ $? = 1 ] && echo "$newrule" >>$file_output.0

  newrule="add rule $1 filter output meta mark $local_mark accept"
  rule_already_exist "$newrule" $file_output.0
  [ $? = 1 ] && echo "$newrule" >>$file_output.0

  if [ "$1" = "ip" -a $route4_fwd = true ]; then
    newrule="add rule $1 filter forward meta mark $local_mark accept"
    rule_already_exist "$newrule" $file_output.0
    [ $? = 1 ] && echo "$newrule" >>$file_output.0
  fi
  if [ "$1" = "ip6" -a $route6_fwd = true ]; then
    newrule="add rule $1 filter forward meta mark $local_mark accept"
    rule_already_exist "$newrule" $file_output.0
    [ $? = 1 ] && echo "$newrule" >>$file_output.0
  fi
}

# Apply local nat routing rules
#
mark_rules_nat_local() {
  [ "$1" = "ip" ] && tmpip=${routing_gw[0]} || tmpip=${routing_gw[1]}

  newrule="add rule $1 nat output meta mark $local_mark dnat to $tmpip"
  rule_already_exist "$newrule" $file_output.0
  [ $? = 1 ] && echo "$newrule" >>$file_output.0
}

# Apply local mangle routing rules
#
mark_rules_mangle_local() {
  [ "$1" = "ip" ] && tmpip=${routing_ip[0]} || tmpip=${routing_ip[1]}
  [ $debug -gt 0 ] && echo "$myname - Routing IP is $tmpip"

  newrule="add rule $1 mangle input iif \"$routing_iface\" $1 daddr { $tmpip } meta mark set $local_mark ct mark set meta mark"
  rule_already_exist "$newrule" $file_output.0
  [ $? = 1 ] && echo "$newrule" >>$file_output.0

  newrule="add rule $1 mangle output $1 saddr { $tmpip } meta mark set $local_mark ct mark set meta mark"
  rule_already_exist "$newrule" $file_output.0
  [ $? = 1 ] && echo "$newrule" >>$file_output.0
}

# Routing
#
routing_rules() {
  [ $debug -gt 0 ] && echo "$myname - Setting ROUTING rules for $1"

  # Remove any routing stuff then add starting mark line
  #
  delete_routing $1
  echo "# Begin Routing $1 by sfw - DON'T REMOVE THIS LINE" >>$file_output.0 && echo "#" >>$file_output.0

  case "$1" in
    "ip")
      for tmpip in $local4_net; do
        $myip r replace $tmpip dev $local_iface metric 256
      done

      # Create and populate a set for destinations ips/nets not using marked routing (only if any)
      #
      newrule="add set ip mangle routing4_nomark { type ipv4_addr; flags interval; }"
      rule_already_exist "$newrule" $file_output.0
      [ $? = 1 ] && echo "$newrule" >>$file_output.0

      if [ "x$routing4_nomark" != "x" ]; then
        newrule="add element $1 mangle routing4_nomark { $routing4_nomark }"
        rule_already_exist "$newrule" $file_output.0
        [ $? = 1 ] && echo "$newrule" >>$file_output.0
      fi

      # Create and populate a set for local nets (only if any)
      #
      newrule="add set ip mangle local4_net { type ipv4_addr; flags interval; }"
      rule_already_exist "$newrule" $file_output.0
      [ $? = 1 ] && echo "$newrule" >>$file_output.0

      if [ "x$local4_net" != "x" ]; then
        newrule="add element $1 mangle local4_net { $local4_net }"
        rule_already_exist "$newrule" $file_output.0
        [ $? = 1 ] && echo "$newrule" >>$file_output.0
      fi

      # Create local rules
      #
      mark_rules_filter_local $1

      # Create nat rules
      #
      mark_rules_nat_local $1

      for tmpfwd in $fwd4_mark; do
        tmpip=$(echo $tmpfwd|grep "|"|cut -d \| -f1)
        [ "x$tmpfwd" != "x" ] && tmpmark=$(hex8digits $(echo $tmpfwd|grep "|"|cut -d \| -f2)) && mark_nat_rules $1 $tmpip $tmpmark
      done

      # Create mangle rules
      #
      mark_rules_mangle_local $1

      for tmpfwd in $fwd4_mark; do
        tmpip=$(echo $tmpfwd|grep "|"|cut -d \| -f1)
        [ "x$tmpfwd" != "x" ] && tmpmark=$(hex8digits $(echo $tmpfwd|grep "|"|cut -d \| -f2)) && mark_mangle_rules_src $1 $tmpip $tmpmark
      done
      [ $route4_fwd = true ] && mark_rules_mangle $1 ${routing_net[0]} $routing_mark local4_net

      tmpmark=$(hex8digits $(echo $local_mark|cut -d x -f2))
      newrule="add rule $1 mangle prerouting iif \"$local_iface\" $1 saddr { ${routing_net[0]} } $1 daddr != @routing4_nomark meta mark set $tmpmark ct mark set meta mark"
      rule_already_exist "$newrule" $file_output.0
      [ $? = 1 ] && echo "$newrule" >>$file_output.0
      ;;
    "ip6")
      for tmpip in $local6_net; do
        $myip -6 r replace $tmpip dev $local_iface metric 256
      done

      # Create and populate a set for destinations ips/nets not using marked routing (only if any)
      #
      newrule="add set ip6 mangle routing6_nomark { type ipv6_addr; flags interval; }"
      rule_already_exist "$newrule" $file_output.0
      [ $? = 1 ] && echo "$newrule" >>$file_output.0

      if [ "x$routing6_nomark" != "x" ]; then
        newrule="add element $1 mangle routing6_nomark { $routing6_nomark }"
        rule_already_exist "$newrule" $file_output.0
        [ $? = 1 ] && echo "$newrule" >>$file_output.0
      fi

      # Create and populate a set for local nets (only if any)
      #
      newrule="add set ip6 mangle local6_net { type ipv6_addr; flags interval; }"
      rule_already_exist "$newrule" $file_output.0
      [ $? = 1 ] && echo "$newrule" >>$file_output.0

      if [ "x$local6_net" != "x" ]; then
        newrule="add element $1 mangle local6_net { $local6_net }"
        rule_already_exist "$newrule" $file_output.0
        [ $? = 1 ] && echo "$newrule" >>$file_output.0
      fi

      # Create local rules
      #
      mark_rules_filter_local $1

      # Create nat rules
      #
      mark_rules_nat_local $1

      for tmpfwd in $fwd6_mark; do
        tmpip=$(echo $tmpfwd|grep "|"|cut -d \| -f1)
        [ "x$tmpfwd" != "x" ] && tmpmark=$(hex8digits $(echo $tmpfwd|grep "|"|cut -d \| -f2)) && mark_rules_nat $1 $tmpip $tmpmark
      done

      # Create mangle rules
      #
      mark_rules_mangle_local $1

      for tmpfwd in $fwd6_mark; do
        tmpip=$(echo $tmpfwd|grep "|"|cut -d \| -f1)
        [ "x$tmpfwd" != "x" ] && tmpmark=$(hex8digits $(echo $tmpfwd|grep "|"|cut -d \| -f2)) && mark_rules_mangle_src $1 $tmpip $tmpmark
      done
      [ $route6_fwd = true ] && mark_rules_mangle $1 ${routing_net[1]} $routing_mark local6_net

      tmpmark=$(hex8digits $(echo $local_mark|cut -d x -f2))
      newrule="add rule $1 mangle prerouting iif \"$local_iface\" $1 saddr { ${routing_net[1]} } $1 daddr != @routing6_nomark meta mark set $tmpmark ct mark set meta mark"
      rule_already_exist "$newrule" $file_output.0
      [ $? = 1 ] && echo "$newrule" >>$file_output.0
      ;;
    *)
      ;;
  esac

  # Add final mark line & empty line
  #
  echo "#" >>$file_output.0 && echo "# End Routing by sfw - DON'T REMOVE THIS LINE" >>$file_output.0 >>$file_output.0

  [ $debug -gt 0 ] && echo "$myname - ROUTING rules setted for $1"
}

# Create tables
#
tables() {
    echo "add table $1 filter" >>$2
    echo "add table $1 mangle" >>$2
    echo "add table $1 raw" >>$2
    echo "add table $1 nat" >>$2
}

# Create chains
#
chains() {
    echo "add chain $1 filter input { type filter hook input priority 0 ; policy $2 ; }" >>$3
    echo "add chain $1 filter output { type filter hook output priority 0 ; policy $2 ; }" >>$3
    echo "add chain $1 filter forward { type filter hook forward priority 0 ; policy $2 ; }" >>$3
    echo "add chain $1 mangle input { type filter hook input priority mangle ; policy accept ; }" >>$3
    echo "add chain $1 mangle output { type route hook output priority mangle ; policy accept ; }" >>$3
    echo "add chain $1 mangle forward { type filter hook forward priority mangle ; policy accept ; }" >>$3
    echo "add chain $1 mangle prerouting { type filter hook prerouting priority mangle ; policy accept ; }" >>$3
    echo "add chain $1 mangle postrouting { type filter hook postrouting priority mangle ; policy accept ; }" >>$3
    echo "add chain $1 nat input { type nat hook input priority 100 ; policy accept ; }" >>$3
    echo "add chain $1 nat output { type nat hook output priority 100 ; policy accept ; }" >>$3
    echo "add chain $1 nat prerouting { type nat hook prerouting priority dstnat ; policy accept ; }" >>$3
    echo "add chain $1 nat postrouting { type nat hook postrouting priority srcnat ; policy accept ; }" >>$3
    echo "add chain $1 nat default_postrouting { type nat hook postrouting priority 100 ; policy accept ; }" >>$3
}

# Create sets and maps
#
setsAndMaps() {
    tmp_output=$myoutput/$1/rules.$IFACE

    [ "$1" = "ip6" ] && mytype="ipv6_addr" || mytype="ipv4_addr"

    # Create sets
    #
    echo "add set $1 filter world_tcp.$IFACE { type inet_service; flags interval; }" >>$tmp_output
    echo "add set $1 filter world_udp.$IFACE { type inet_service; flags interval; }" >>$tmp_output
    echo "add set $1 filter local_tcp.$IFACE { type inet_service; flags interval; }" >>$tmp_output
    echo "add set $1 filter local_udp.$IFACE { type inet_service; flags interval; }" >>$tmp_output
    echo "add set $1 filter redirect_tcp.$IFACE { type inet_service; flags interval; }" >>$tmp_output
    echo "add set $1 filter redirect_udp.$IFACE { type inet_service; flags interval; }" >>$tmp_output
    echo "add set $1 filter local_nets.$IFACE { type $mytype; flags interval; }" >>$tmp_output
    echo "add set $1 nat private_ipv4.$IFACE { type $mytype; flags interval; }" >>$tmp_output
    [ $resetRules = true ] && echo "add set $1 nat blocklist { type $mytype; flags interval; }" >>$myoutput/$1/rules

    # Create maps
    #
    if [ $nftnumvers -ge 100 ]; then
        # nft version greater or equal v1.0.0
        echo "add map $1 nat forward_tcp.$IFACE { type $mytype . inet_service : $mytype . inet_service; flags interval; }" >>$tmp_output
        echo "add map $1 nat forward_udp.$IFACE { type $mytype . inet_service : $mytype . inet_service; flags interval; }" >>$tmp_output
	else
  	  # nft version less than v1.0.0
  	  echo "add map $1 nat fwdtoip_tcp.$IFACE { type inet_service : $mytype; flags interval; }" >>$tmp_output
  	  echo "add map $1 nat fwdtoip_udp.$IFACE { type inet_service : $mytype; flags interval; }" >>$tmp_output
  	  echo "add map $1 nat fwdtoport_tcp.$IFACE { type inet_service : inet_service; flags interval; }" >>$tmp_output
  	  echo "add map $1 nat fwdtoport_udp.$IFACE { type inet_service : inet_service; flags interval; }" >>$tmp_output
    fi
    echo "add map $1 nat redirect_tcp.$IFACE { type inet_service : inet_service; flags interval; }" >>$tmp_output
    echo "add map $1 nat redirect_udp.$IFACE { type inet_service : inet_service; flags interval; }" >>$tmp_output
    echo "add map $1 nat blocklist_tcp.$IFACE { type $mytype . inet_service  : verdict; flags interval; }" >>$tmp_output
    echo "add map $1 nat blocklist_udp.$IFACE { type $mytype . inet_service  : verdict; flags interval; }" >>$tmp_output
}

# Countracks
#
setct() {
    tmp_output=$myoutput/$1/rules.$IFACE

    echo "add rule $1 filter input iif $IFACE ct $defaultct counter accept" >>$tmp_output
    echo "add rule $1 filter input iif $IFACE ct state invalid drop" >>$tmp_output
    echo "add rule $1 filter forward iif $IFACE ct $defaultct,new counter accept" >>$tmp_output
    echo "add rule $1 filter output oif $IFACE ct $defaultct,new counter accept" >>$tmp_output
    echo "add rule $1 nat prerouting iif $IFACE ct status dnat accept" >>$tmp_output
}

# Test if a function exists
#
function_exists_and_iface_is_valid() {
    isifacevalidAndUp                        # check if interface is valid and up
    mysearch=$(grep -e "$1()" $local_datas | tr -d " " | cut -d { -f 1)  # we add () after parameter $1 as we are looking for a function

    if [ $ifaceok = true ]; then
        if [ "${mysearch#1}" = "$1()" ]; then
            IFACE=$(echo $1| cut -d _ -f2)
            interface_$IFACE
            [ $debug -gt 0 ] && echo "$myname - function $1 exist, we treat interface \"$IFACE\" as parallel"
        else
            [ $debug -gt 0 ] && echo "$myname - $1 is valid but not a function, maybe main interface ?"
            interface_main
            [ $debug -gt 0 ] && echo "$myname - We treat \"$IFACE\" as main interface"
        fi
    else
        # interface not valid
        [ $debug -gt 0 ] && echo "$myname - interface $IFACE is NOT valid, EXIT"
        exit 0
    fi

    return 0
}

# Main interface setup
#
interface_main() {
    if [ "x$IFACE" = "x" ]; then
        IFACE=$(ip link show $fallbackIFACE|grep "state UP")     # IFACE not defined, try if fallback is UP and use it. If not, exit
        [ "x$IFACE" = "x"  ] && echo "$myname - No INTERFACE setted nor default $fallbackIFACE found, EXIT" && exit 0
        IFACE=$fallbackIFACE
        [ $debug -gt 0   ] && echo "$myname - fallbackIFACE $fallbackIFACE used"

        isifacevalidAndUp                    # check if interface is valid and up
        [ $ifaceok = false ] && echo "$myname - fallbackIFACE $fallbackIFACE not valid, EXIT" && exit 0
    fi

# Check if the IFACE match the one we defined
#
    if [ "x$onlyforIFACE" != "x" -a "$onlyforIFACE" != "$IFACE" ]; then
        [ $debug -gt 0 ] && echo "$myname - INTERFACE ($IFACE) doesn't match the one we are waiting ($onlyforIFACE)"
        exit 0
    fi

# Do we take care of fail2ban table, chain and sets
#
    if [ "$1" = "onlyf2b" ]; then
        f2b=$1
        IFACE=$fallbackIFACE
    fi

    [ $debug -gt 0 ] && [ "$f2b" = "onlyf2b" ] && echo "$myname - fail2ban will be treated"

    return 0
}

getips() {
    addripv4=`ip a show $IFACE|grep "inet "|grep global|tr -s " "|cut -d " " -f 3|cut -d / -f 1`
    addripv6=`ip a show $IFACE|grep -E "inet6"|grep global|tr -s " "|cut -d " " -f 3|cut -d / -f 1`
    addripv4=`echo $addripv4|tr \n " "|tr , " "|tr -s " "`
    addripv6=`echo $addripv6|tr \n " "|tr , " "|tr -s " "`

    if [ "$1" = "ip4" -a "x$addripv4" != "x" -a "`echo $noip4IIF|grep $IFACE`" != "" ]; then
      [ $debug -gt 0 ] && echo "$myname - $IFACE not treated as existing in noip4IIF ($noip4IIF)"
      exit 0
    fi

    if [ "$1" = "ip6" -a "x$addripv6" != "x" -a "`echo $noip6IIF|grep $IFACE`" != "" ]; then
      [ $debug - gt 0 ] && echo "$myname - $IFACE not treated as existing in noip6IFF ($noip6IIF)"
      exit 0
    fi

    [ $debug -gt 0 ] && echo "$myname - Working directory=$PWD" && echo "$myname - FW on $IFACE"

    return 0
}

isifacevalidAndUp() {
# Test if IFACE is a valid interface
    ifaceok=false

    while [ ! -e /sys/class/net/$IFACE ]
    do
        gosleep
        [ "$elapsedtime" = "1" ] && echo "$myname - Device $IFACE does not exists, EXIT" && ifaceok=false && exit 0
    done

    myifup=`echo $(ip link show up | grep $IFACE)`

    while [ "x$myifup" = "x" ]
    do
        [ "$debug"  = "1" ] && echo "$myname - Arguments(#2)=<$1> <$2> - Working directory=$PWD" && echo "$myname - $IFACE not UP"
        gosleep
        [ "$elapsedtime" = "1" ] && exit 0 || myifup=`echo $(ip link show up | grep $IFACE)`
    done

    # Interface is up
    ifaceok=true
    [ $debug -gt 0 ] && echo "$myname - Device $IFACE exists"

    return 0
}

# Sleep
#
gosleep() {
    [ $debug -gt 0 ] && echo "$myname - timetosleep=$mytimetosleep, max=$maxtosleep"

    elapsedtime=$(echo "$mytimetosleep >= $maxtosleep" | $mybc -l)

    sleep $mytimetosleep
    mytimetosleep=$(echo "$mytimetosleep + $timetosleep" | $mybc)

    return 0
}

# Create user rules file
#
user_file() {
    if [ "$2" = "base" ]; then
        echo "#" >"$myoutput/$1/rules.0"
        echo "# Add your own tables, sets or maps in this file, they will be always loaded" >>"$myoutput/$1/rules.0"
        echo "#" >>"$myoutput/$1/rules.0"
        echo "#" >>"$myoutput/$1/rules.0"
        echo "# IMPORTANT NOTE: files in this directory are readed in alphanumerical order" >>"$myoutput/$1/rules.0"
        echo "#" >>"$myoutput/$1/rules.0"
        echo "#" >>"$myoutput/$1/rules.0"
        echo >>"$myoutput/$1/rules.0"
        echo "# For debugging, adapt, uncomment and run sfw-tool reload" >>"$myoutput/$1/rules.0"
        echo "#" >>"$myoutput/$1/rules.0"
        echo "#add chain $1 mangle TRACE_IN { type filter hook prerouting priority -500 ; policy accept ; }" >>"$myoutput/$1/rules.0"
        echo "#add chain $1 mangle TRACE_OUT { type route hook output priority -500 ; policy accept ; }" >>"$myoutput/$1/rules.0"
        echo "#" >>"$myoutput/$1/rules.0"
        echo "#" >>"$myoutput/$1/rules.0"
        echo "#add rule $1 mangle TRACE_IN iif "wg0" $1 daddr { 2001:db8::1 } meta nftrace set 1" >>"$myoutput/$1/rules.0"
        echo "#add rule $1 mangle TRACE_OUT oif "eth0" $1 saddr { 2001:db8::1 } meta nftrace set 1" >>"$myoutput/$1/rules.0"
        echo "" >>"$myoutput/$1/rules.0"
    else
        : >"$myoutput/$1/rules.z" || true
        echo "#" >"$myoutput/$1/rules.z"
        echo "# Add your own rules in this file, they will be always loaded" >>"$myoutput/$1/rules.z"
        echo "#" >>"$myoutput/$1/rules.z"
        echo "#" >>"$myoutput/$1/rules.z"
        echo "" >>"$myoutput/$1/rules.z"
    fi
}

# Check if a rule is already existing
# Param1=rule to check
# Param2=rules file
#
rule_already_exist() {
    [ "$(fgrep "$1" "$2")" = "$1" ] && return 0 || return 1
}
