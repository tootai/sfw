#
# sfw - Initialisation functions - called by /etc/default/sfw (sfw.default)
#
# Copyright (C) 2020-2024 (Daniel Huhardeaux devel@tootai.net)
#
# Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence Publique Générale GNU
# publiée par la Free Software Foundation (version 2 ou bien toute autre version ultérieure choisie par vous).
#
# Ce programme est distribué car potentiellement utile, mais SANS AUCUNE GARANTIE, ni explicite ni implicite, y compris les garanties
# de commercialisation ou d'adaptation dans un but spécifique. Reportez-vous à la Licence Publique Générale GNU pour plus de détails.
#
# Vous devez avoir reçu une copie de la Licence Publique Générale GNU en même temps que ce programme;
# si ce n'est pas le cas, écrivez à la Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, États-Unis.
# ou visitez <https://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

myversion="0.2.4"                                             # Version
nftversion=$(nft -v|cut -d "v" -f2|cut -d " " -f1)
nftnumvers=$(echo $nftversion|tr -d ".")
kernelrelease=$(uname -r)
kernelnumvers=$(echo $kernelrelease|cut -d "-" -f1| sed 's/\.//g')

if [ "$showversion" == "yes" ]; then
    clear
    echo
    echo "$myname - v$myversion, nft v$nftversion (numerical $nftnumvers) running on kernel $kernelrelease (numerical $kernelnumvers)"
    echo
    exit 0
fi

mysystemctl=$(whereis systemctl|grep bin|cut -d \  -f2)       # systemctl binarie
myreaction=$(whereis reaction|grep bin|cut -d \  -f2)         # reaction  binarie

##########################################
#                                        #
# Various checks depending on USER datas #
#                                        #
##########################################

[ $debug -gt 0 ] && echo "$myname - Begin of GENERAL settings"

addripv4=                                                     # variable init
addripv6=                                                     # variable init
addripv4to6_4debug=                                           # variable init
privateipv4nets="::ffff:10.0.0.0/104 ::ffff:172.16.0.0/108 ::ffff:192.168.0.0/112"   # Private ipv4toipv6 networks
with_routing=0                                                # =0 no routing =1 routing ipv4 =2 routing ipv6 =3 routing ipv4 & ipv6
route4_mark=false                                             # route_ID not defined in fwd4_mark
route6_mark=false                                             # route_ID not defined in fwd6_mark
route4_fwd=true                                               # we forward the net traffic to IP with mark from route_ID
route6_fwd=true                                               # we forward the net traffic to IP with mark from route_ID
routing_enabled=$(echo $routing_enabled|sed 's/[A-Z]/\L&/g')  # lower case

[ $debug -gt 0 ] && echo "$myname - Begin of VARIOUS checks"

# Check if nft, bc ip and sed programs are installed
#
fwtables=$(whereis nft|grep bin|cut -d " " -f2) || true
[ "x$fwtables" = "x" ] && echo "$myname - nftables not installed, EXIT" && exit 1

mybc=$(whereis bc|grep bin|cut -d " " -f2) || true
[ "x$mybc" = "x" ] && echo "$myname - bc not installed, EXIT" && exit 1

myip=$(whereis ip|grep bin|cut -d " " -f2) || true
[ "x$myip" = "x" ] && echo "$myname - ip not installed, routing disabled" && routing_enabled="no"

mysed=$(whereis sed|grep bin|cut -d " " -f2) || true
[ "x$mysed" = "x" ] && echo "$myname - sed not installed, routing disabled" && routing_enabled="no"

# check if directory rules exits and create subdirectories and include file if not existing
#
[ ! -d "$myoutput" ] && mkdir $myoutput && [ $debug -gt 0 ] && echo "$myname - Create directory $myoutput"
[ ! -d "$myoutput/inet" ] && mkdir "$myoutput/inet" && [ $debug -gt 0 ] && echo "$myname - Create directory $myoutput/inet"
[ ! -d "$myoutput/ip"   ] && mkdir "$myoutput/ip"   && [ $debug -gt 0 ] && echo "$myname - Create directory $myoutput/ip"
[ ! -d "$myoutput/ip6"  ] && mkdir "$myoutput/ip6"  && [ $debug -gt 0 ] && echo "$myname - Create directory $myoutput/ip6"
if [ ! -e "$myoutput/nft.rules" ]; then
  echo "include \"inet/rules*\"" > "$myoutput/nft.rules" && [ $debug -gt 0 ] && echo "$myname - Create file $myoutput/nft.rules"
  echo "include \"ip/rules*\""  >> "$myoutput/nft.rules"
  echo "include \"ip6/rules*\"" >> "$myoutput/nft.rules"
fi
if [ ! -e "$myoutput/nft.rules-4" ]; then
  echo "include \"inet/rules*\"" > "$myoutput/nft.rules-4" && [ $debug -gt 0 ] && echo "$myname - Create file $myoutput/nft.rules-4"
  echo "include \"ip/rules*\""  >> "$myoutput/nft.rules-4"
fi
[ ! -e "$myoutput/nft.rules-6" ] && echo "include \"ip6/rules*\"" > "$myoutput/nft.rules-6" && [ $debug -gt 0 ] && echo "$myname - Create file $myoutput/nft.rules-6"

#
# Functions
#
# Function for parallel interfaces (if any)
#
#interface_INTERFACE_NAME() {
# for example for INTERFACE_NAME=eth10 it would be interface_eth10() { }
#
# ALL user datas can be adapted here for THIS interface
# They will overwrite the previous one

  # Usually only below ones are interesting to adapt
  #
  #debug=0
  #donat=
  #nets4=
  #nets6=
  #net4tcpports=
  #net6tcpports=
  #net4udpports=
  #net6udpports=
  #world4tcpports=
  #world6tcpports=
  #world4udpports=
  #world6udpports=
  #denied4ipports=
  #denied6ipports=
  #ports4fwd=
  #ports6fwd=

  #return 0
#}

# Example
# same configuration that main interface
#
#interface_enp6s0() {
#  return 0
#}

# Setting up IP spoofing protection
# $1=1 enable =0 disable

ReversePathFiltering() {
  if [ -e /proc/sys/net/ipv4/conf/all/rp_filter ]
  then
    for f in /proc/sys/net/ipv4/conf/*/rp_filter
    do
      echo $1 > $f
    done
  fi
}

# format routing table
# Replace TABs with space, squeeze spaces
#
format_routing_table() {
  myrouting_table=$(echo $routing_table|sed 's/\x09/\ /g'|tr -s " ")
}

# format list variables
#
format_list_variables() {
  # Replace TABs with space, squeeze spaces
  myrouting_table=$(echo $myrouting_table|sed 's/\x09/\ /g'|tr -s " ")

  # Replace squeeze spaces, replace spaces with coma, squeeze coma
  local4_net=$(echo $local4_net|tr -s " "|sed 's/\ /,/g'|tr -s ",")
  local6_net=$(echo $local6_net|tr -s " "|sed 's/\ /,/g'|tr -s ",")
  fwd4_mark=$(echo $fwd4_mark|tr -s " "|sed 's/\ /,/g'|tr -s ",")
  fwd6_mark=$(echo $fwd6_mark|tr -s " "|sed 's/\ /,/g'|tr -s ",")
  routing4_nomark=$(echo $routing4_nomark|tr -s " "|sed 's/\ /,/g'|tr -s ",")
  routing6_nomark=$(echo $routing6_nomark|tr -s " "|sed 's/\ /,/g'|tr -s ",")
}

# convert hexa to 8 bits
#
hex8digits() {
  # return the hex value passed removing prefix 0x (if any)
  # convert value du decimal then back to hex in 8 bits
  # 
  echo $(printf '0x%08x\n' $(echo $((16#$(echo $1|cut -d x -f2)))))
}

# remove existing routing rules
#
delete_routing() {
  if [ "$1" == "ip" -o "$1" == "inet" -o "$1" == "all" ]; then 
    $mysed -i '/^#.*Begin.*Routing.*by.*sfw.*/,/^#.*End.*Routing.*by.*sfw.*/d' $myoutput/inet/rules.0 2>/dev/null
    $mysed -i '/^#.*Begin.*Routing.*by.*sfw.*/,/^#.*End.*Routing.*by.*sfw.*/d' $myoutput/ip/rules.0 2>/dev/null
  fi

  if [ "$1" == "ip6" -o "$1" == "all" ]; then
    $mysed -i '/^#.*Begin.*Routing.*by.*sfw.*/,/^#.*End.*Routing.*by.*sfw.*/d' $myoutput/ip6/rules.0 2>/dev/null
  fi
}

# remove existing table rules
#
delete_tables() {
  # format old datas from perhaps existing table rules & routes to delete them
  #
  format_routing_table
  mytable=$(echo $routing_table|cut -d " " -f2)

  if [ "$1" == "ip" -o "$1" == "inet" -o "$1" == "all" ]; then 
    for tmpip in $local4_net; do
      $myip r del $tmpip dev $local_iface metric 256 2>/dev/null
    done

    # delete existing settings (if any) and still defined
    # 
    $myip rule flush table $mytable 2>/dev/null
    $myip r flush table $mytable 2>/dev/null
  fi

  if [ "$1" == "ip6" -o "$1" == "all" ]; then
    for tmpip in $local6_net; do
      $myip -6 r del $tmpip dev $local_iface metric 256 2>/dev/null
    done

    # delete existing settings (if any) and still defined
    # 
    $myip -6 rule flush table $mytable 2>/dev/null
    $myip -6 r flush table $mytable 2>/dev/null
  fi
}

# iproute2 & nftable routing if enabled
#
set_routing() {
  #
  # Called without parameter, $myname is the determinator
  # . parameter $1 with value ip  is equivalent to $myname=sfw4
  # . parameter $1 with value ip6 is equivalent to $myname=sfw6
  # . parameter $1 with value all is equivalent to $myname=sfw4 & sfw6
  #
  # Check if route table exist in iproute2, if not we create it
  # Replace TABs with space and squeeze spaces
  #
  [ $debug -gt 0 ] && echo "$myname - Begin setting ROUTING routes"

  format_list_variables

  if [ "$(cat $iproute2|sed 's/\x09/\ /g'|tr -s " "|grep "$myrouting_table")" != "$myrouting_table" ]; then
    # table & ID not existing, we create it
    #
    echo "" >> $iproute2
    echo "# Added by sfw routing procedure" >> $iproute2
    echo "$myrouting_table" >> $iproute2
    [ $debug -gt 0 ] && echo "$myname - Create TABLE $myrouting_table in $iproute2"
  else
    # Table exist, flush rules and routes, ipv4 & ipv6
    #
    mytable=$(echo $myrouting_table|cut -d " " -f2)
    [ "$myname" == "sfw4" -o "$1" == "ip"  -o "$1" == "all" ] && $myip rule flush table $mytable 2>/dev/null
    [ "$myname" == "sfw4" -o "$1" == "ip"  -o "$1" == "all" ] && $myip r flush table $mytable 2>/dev/null
    [ "$myname" == "sfw6" -o "$1" == "ip6" -o "$1" == "all" ] && $myip -6 rule flush table $mytable 2>/dev/null
    [ "$myname" == "sfw6" -o "$1" == "ip6" -o "$1" == "all" ] && $myip -6 r flush table $mytable 2>/dev/null
    [[ $debug -gt 0 && ("$myname" == "sfw4" || "$1" == "ip"  || "$1" == "all") ]] && echo "$myname - ip rule flush table $mytable"\
      && echo "$myname - ip r flush table $mytable"
    [[ $debug -gt 0 && ("$myname" == "sfw6" || "$1" == "ip6" || "$1" == "all") ]] && echo "$myname - ip -6 rule flush table $mytable"\
      && echo "$myname - ip -6 r flush table $mytable"
  fi

  if [ "$routing_enabled" == "iproute" ]; then
    delete_routing all

    if [ "$myname" == "sfw4" -o "$1" == "ip" -o "$1" == "all" ]; then
      $myip rule add from ${routing_ip[0]} table $mytable
      $myip r replace default via ${routing_gw[0]} dev $routing_iface table $mytable
      [ $debug -gt 0 ] && echo "$myname - ip rule add from ${routing_ip[0]} using gw ${routing_gw[0]} dev $routing_iface to table $mytable"
    fi

    if [ "$myname" == "sfw6" -o "$1" == "ip6" -o "$1" == "all" ]; then
      $myip -6 rule add from ${routing_ip[1]} table $mytable
      $myip -6 r replace default via ${routing_gw[1]} dev $routing_iface table $mytable
      [ $debug -gt 0 ] && echo "$myname - ip -6 rule add from ${routing_ip[1]} using gw ${routing_gw[1]} dev $routing_iface to table $mytable"
    fi

    # stuff done, end procedure
    #
    return 0
  fi

  routing_mark=$(hex8digits $routing_mark)
  local_mark=$(hex8digits $local_mark)
  [ $debug -gt 0 ] && echo "$myname - routing mark is now $routing_mark, local mark is now $local_mark" 

  if [ "$myname" == "sfw4" -o "$1" == "ip" -o "$1" == "all" ]; then
    if [ $route4_mark == true ]; then
      # add local rule and route for ip4 in routing table
      #
      $myip rule add from all fwmark $local_mark table $mytable
      $myip rule add from all fwmark $routing_mark table $mytable
      [ $debug -gt 0 ] && echo "$myname - ip rule add from all $local_mark & $routing_mark table $mytable"

      $myip r replace default dev $routing_iface table $mytable
      $myip route flush cache
      [ $debug -gt 0 ] && echo "$myname - ip route add table $mytable"
    fi 

    if [ "x$fwd4_mark" != "x" -a "x$fwd4_mark" != "xnone" ]; then
      # add forward rule for ip4 in routing table
      #
      for tmpip in $fwd4_mark; do
        mymark=$(echo $tmpip|grep "|"|cut -d \| -f2)
        $myip rule add from all fwmark $(hex8digits $mymark) table $mytable
        [ $debug -gt 0 ] && echo "$myname - ip rule add from all $(hex8digits $mymark) table $mytable"
      done
    fi

    [ $debug -gt 0 ] && echo "$myname - ip r replace default dev $routing_iface table $mytable" && echo "$myname - ip r flush cache"

    # check if route[46]_net are defined in local[46]_net
    # if YES, no forwarding rules will be applied
    # 
    for tmpnet in $local4_net; do
      [ "x$tmpnet" == "x${routing_net[0]}" ] && route4_fwd=false
    done

    [ $route4_fwd == true -a "x${routing_net[0]}" != "x" ] && with_routing=$((with_routing+1))
  fi

  if [ "$myname" == "sfw6" -o "$1" == "ip6" -o "$1" == "all" ]; then
    if [ $route6_mark == true ]; then
      # add local rule and route for ip6 in routing table
      #
      $myip -6 rule add from all fwmark $local_mark table $mytable
      $myip -6 rule add from all fwmark $routing_mark table $mytable
      [ $debug -gt 0 ] && echo "$myname - ip -6 rule add from all $local_mark & $routing_mark table $mytable"

      $myip -6 r replace default dev $routing_iface table $mytable
      $myip -6 r flush cache
      [ $debug -gt 0 ] && echo "$myname - ip -6 route add table $mytable"
    fi

    if [ "x$fwd6_mark" != "x" -a "x$fwd6_mark" != "xnone" ]; then
      # add rule and route for ip6 in routing table
      #
      for tmpip in $fwd6_mark; do
        mymark=$(echo $tmpip|grep "|"|cut -d \| -f2)
        $myip -6 rule add from all fwmark $(hex8digits $mymark) table $mytable
        [ $debug -gt 0 ] && echo "$myname - ip -6 rule add from all $(hex8digits $mymark) table $mytable"
      done
    fi

    [ $debug -gt 0 ] && echo "$myname - ip -6 r replace default dev $routing_iface table $mytable" && echo "$myname - ip -6 r flush cache"

    # sfw-iproute job ended here
    #
    [ "x$1" != "x" ] && exit 0

    # check if route[46]_net are defined in local[46]_net
    # if YES, no forwarding rules will be applied
    # 
    for tmpnet in $local6_net; do
      [ "x$tmpnet" == "x${routing_net[1]}" ] && route6_fwd=false
    done

    [ $route6_fwd == true -a "x${routing_net[1]}" != "x" ] && with_routing=$((with_routing+2))
  fi

  [ $debug -gt 0 ] && echo "$myname - ROUTING routes analyzed, routing value being $with_routing"
}

#
# MAIN program
#

if [ "$myname" != "sfw-tool" -a "$myname" != "sfw-iproute" -a "$(expr substr $myname 1 3)" = "sfw" ]; then
  [ $debug -gt 0 ] && echo "$myname - Begin of INTERFACE checks"

  [ "x$IFACE" = "x" ] && IFACE=$1                 # Set interface if not already done (NM or ifup)
  [ "x$IFACE" = "x" ] && IFACE=$fallbackIFACE     # Set interface if not already done

  # Not setting fw rules if no interface given or set for local or all
  #
  if [ "x$IFACE" = "x" -o "$IFACE" = "lo" -o "$IFACE" = "--all" ]; then
    [ $debug -gt 0 ] && echo "$myname - INTERFACE ($IFACE) name empty or not allowed"
    exit 0
  fi

  [ "x$f2b" = "x"      ] && f2b=$(echo $1$2$3$4|grep onlyf2b)           # fail2ban not setted, check if f2b or onlyf2b is passed as parameter       
  [ "x$f2b" = "x"      ] && f2b=$(echo $1$2$3$4|grep f2b)               # onlyf2b not found, check for f2b
  [ "x$reaction" = "x" ] && reaction=$(echo $1$2$3$4|grep reaction)     # reaction not setted, check if passed as parameter
  backupIFS=$IFS                                                        # Backup of current IFS

  # Not setting fw rules if IFACE ist not the only one to treat
  #
  if [ "x$onlyforIFACE" != "x" -a `echo "$IFACE"|cut -d = -f2` != "$onlyforIFACE" ]; then
    [ $debug -gt 0 ] && echo "$myname - INTERFACE ($IFACE) not in list to protect"
    exit 0
  fi

  # Load ipv6 private organisation if existing
  #
  # Getting IPs *ONLY* if we're setting rules
  #
  if [ "$myname" = "sfw4" -o "$myname" = "sfw6" ]; then
    if [ "$sitePrefix" != "none" ]; then
      addripv6_local=`ip a show $IFACE|grep "inet6 $(echo $sitePrefix|cut -d \: -f1)"|grep global|tr -s " "|cut -d " " -f 3|cut -d / -f 1`
      addripv6_local=`echo $addripv6_local|tr \n ,|tr \  ,`
    fi
  fi

  [ "x$addripv6_local" = "x" ] && addripv6_local="none"

  if [ "$routing_enabled" == "iproute" -a "$myname" != "sfw" -a "$myname" != "sfw-tool" ];then
    # Replace TABs with space and squeeze spaces
    #
    format_routing_table

    myproto=
    [ "x${routing_net[0]}" != "x" ] && myproto=ip && with_routing=1
    [ "x${routing_net[1]}" != "x" -a "$myproto" == "ip" ] && myproto=all || [ "x${routing_net[1]}" != "x" ] && myproto=ip6 

    # Check if local and route IFACE are existing
    # Set routing if OK
    #
    test -e /sys/class/net/$routing_iface && set_routing $myproto || \
          echo "$myname - Routing ABORTED >>> Missing routing interface ($routing_iface) !!!"
  else
    if [ "x$routing_table" != "x" -a "$routing_enabled" == "yes" -a "$(echo "$myname"|grep "[46]")" = "$myname" ]; then
      # Check if routing_mark is existing in fwd_mark
      #
      # If not, we check if local_mark is defined. In this
      # case, we treat ONLY local route, otherwise no routing
      #
      myrouting_mark=$(echo $routing_mark|cut -d x -f2)

      for tmpnet in $fwd4_mark; do
        tmpmark=$(echo $tmpnet|grep "|"|cut -d \| -f2|cut -d x -f2) 
        [ "$myrouting_mark" = "$tmpmark" ] && route4_mark=true
      done

      for tmpnet in $fwd6_mark; do
        tmpmark=$(echo $tmpnet|grep "|"|cut -d \| -f2|cut -d x -f2) 
        [ "x$tmpmark" != "x" ] && route6_mark=true
      done

      if [ "x$local_mark" = "x" ]; then
        [ $route4_mark = false -a "$myname" = "sfw4" ] && echo "$myname - Routing ABORTED >>> No ip4 FORWARD mark found for route_ID $myroute"  
        [ $route6_mark = false -a "$myname" = "sfw6" ] && echo "$myname - Routing ABORTED >>> No ip6 FORWARD mark found for route_ID $myroute"
      else
        [ "x${routing_net[0]}" != "x" ] && route4_mark=true
        [ "x${routing_net[1]}" != "x" ] && route6_mark=true
      fi

      if [ $route4_mark = true -o $route6_mark = true ] ; then
        # Replace TABs with space and squeeze spaces
        # 
        format_routing_table
 
        # Check if local and route IFACE are existing
        # Set routing if OK
        #
        test -e /sys/class/net/$routing_iface && test -e /sys/class/net/$local_iface && set_routing || \
          echo "$myname - Routing ABORTED >>> Missing route and/or local interface(s) ($routing_iface and/or $local_iface) !!!"
      fi
    fi
  fi

  [ $debug -gt 0 ] && [ "$myname" = "sfw4" -o "$myname" = "sfw6" ] && echo "$myname - Begin of FW settings"
fi

# === Setting forwarding as we can act as router ===
# ===       ACTIVATE here or in sysctl.conf      ===

# For IPv4
#
#echo 1 > /proc/sys/net/ipv4/ip_forward
#

# For IPv6
#
#echo 1 > /proc/sys/net/ipv6/conf/all/forwarding

#
# === END of routing stuff ===
