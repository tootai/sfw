# SimpleFireWall

Série de scripts sh basés sur nftables permettant de gérer les règles de pare-feu sur **unique** ou **multiples** interfaces

Version 0.2.4 - 20240903
- update Copyright à 2020-2024
- bug dans sfw, noip4IFF au lieu de noip4IIF. Idem pour noip6IFF qui devient noip6IIF 
- bug dans sfw-common.sh, la variable $ipv6Prefix n'existe pas,elle est retirée lors de la recherche des adresses ipv6 (...|grep -E "inet6") dans getips
- rajout du passage de paramètre ip4|ip6 à getips afin de tester correctement noip[4|6]IFF dans sfw4/sfw6
  Ceci permet d'exclure des règles de pare-feu l'un des deux protocoles. Ex: docker iptables-nft ne gère que l'ip4, on peut rajouter nos règles ip6
 
Version 0.2.3 - 20240429
- sfw4 sfw6 sfw-tool sfw-init.sh et etc.default.sfw ont été adaptés à la gestion du logiciel reaction https://framagit.org/ppom/reaction-wiki
  à savoir un redémarrage de reaction après un flush des règles du pare-feu
- sfw.default initialise les variables f2b et reaction afin de pouvoir les activer sans forcément passer par les paramètres
- les sets/maps blocklist sont alimentées par les IP définies dans denied[46]ipports afin d'être traitées en nat/prerouting et non plus
  en filter/input

Version 0.2.2.1 - 20240125
- sfw-init.sh bug dans format_list_variables ou routing4_nomark prenait la valeur de routing6_nomark
- sfw.default 0x254 devient la valeur par defaut de routing_mark

Version 0.2.2 - 20231120
- sfw-init.sh rajout de la gestion de version du kernel
- sfw-common.sh nouvelle gestion des ports forwarding en utilisant une clé double
  dans les maps comme "ip6 daddr . udp dport @forward_udp.lan"
  Les maps fwdtoip et fwdtoport ne sont conservées que pour une rétro compatibilité avec nftables version 0.X.Y
- sfw4 adaptation à la nouvelle gestion des ports forwarding
- sfw6 adaptation à la nouvelle gestion des ports forwarding
- sfw.default rajout de la possibilité de gérer du (S)ource nat ou du (M)asquerading pour les règles de forward
  
Version 0.2.1 - 20231005
Le fichier des règles d'une interface peut être complété par son pendant suffixé _custom. Ex:
  [inet|ip|ip6]/rules.eth0 peut être complété manuellement par des règles utilisateur [inet|ip|ip6]/rules.eth0_custom
  Ces fichiers de règles utilisateur seront *SUPPRIMÉS* avec celle de l'interface (commande sfw-tool remove=eth0)
 
- sfw-common.sh création de la fonction de création des tables et chaines
- sfw4 les tables et chaines sont églement créées pour le protocole inet
- sfw4 la table fail2ban n'est créée que si demandée en ligne de commande
- sfw6 adaptation à la nouvelle fonction de création des tables et chaines
- sfw6 la table fail2ban n'est créée que si demandée en ligne de commande
- sfw.default lecture de la demande -passée en paramètre- de création de la table fail2ban
- sfw.default integration de donat dans les paramètres utilisateurs/fonction interface_INTERFACE_NAME()
- sfw.default scindé: les données utilisateur y sont consignés, les fonctions d'initialisation déplacées dans sfw-init.sh
  La mise à niveau de version est facilité, uniquement les nouvelles données utilisateur étant éventuellement à ajouter
- sfw-init.sh contient les fonctions d'initialisation
- sfw remplacement de sfw4ip4/sfw4ip6 par sfwip4 & sfwip6
- sfw-tool le paramètre <what> est géré avant l'appel aux fonctions
- sfw.service remplacement sfw-reset par sfw-tool flush

Version 0.2.0 - 20230816
Le routage ip/ipv6 est géré via le marquage de paquets et/ou iproute2
- sfw.default rajout des variables pour la gestion du routage ips/nets
- sfw.default rajout de fonctions pour la gestion du routage
- sfw4 appel de la fonction marquage de paquets si configuré
- sfw6 appel de la fonction marquage de paquets si configuré
- sfw-common.sh la fonction forward teste si une règle à appliquer ne serait pas déjà définie. Ceci évite les doublons
- sfw-common.sh rajout des fonctions liées au routage
- sfw-iproute Nouveau. gère la création des tables de routage iproute2 en fonction de la configuration définie dans sfw.default
  Ceci permet de recharger les règles existantes lors d'un démarrage au lieu de les regénérer
- suppression de sfw-accept sfwd sfwd.service sfw-monitor

Version 0.1.4 - v20230816
- sfw-tool bug de passage du paramètre $2 pour remove=IFACE
- sfw.default le séparateur de marquage pour openIIF est le | (pipe)
- sfw.default rajout de la partie MAIN programme 
- sfw4 adaptation pour le séparateur de marquage
- sfw6 adaptation pour le séparateur de marquage

Version 0.1.3.1 - 20230708
- sfw.default bug dans l'exemple des interfaces parallèles (nets[4|6] et non net[4|6])
- sfw-common.sh modification du message verbose pour une interface parallèle (retrait de interface_ devant $1)

Version 0.1.3 - 20230629
De nouvelles chain ont été rajoutées dans les tables mangle et nat permettant de gérer le marquage des paquets pour le routing et le traçage
- rajout des chain input, forward, prerouting, postrouting dans la table mangle
- rajout des chain input et output dans la table nat
- modification des priorités en srcnat et dstnat dans les chain prerouting et postrouting
- rajout des chain (commentés) dans TRACE_IN et TRACE_OUT dans la table mangle ainsi que des règles d'exemples pour le suivi trace via nft monitor
  Ces commandes sont à insérer dans [ip|ip6]/rules.0 existants ou alors copier les fichiers rules.0.ip[6] joints à leur emplacement en les renommants
  Ces règles sont créées commentées dans [ip|ip6]/rules.0 en cas de nouvelle installation 
- correction de messages verbeux dans sfw4 et sfw6
- sfw6 bug lors de la gestion multi ip6 GUA d'une interface, on applique à la règle chaque ip6 séparément.
- correction bug broadcast/multicast (accept manquant) dans le fichier sfw4
- sfw.default l'option debug=0/1 est déplacée en début de fichier 
- sfw.default remplacement de l'expression ${myname:0:3} par $(expr substr $myname 1 3) afin d'être compatible avec d'autres interpréteurs que bash
- sfw.default remplacement de $(which ...) par $(whereis ...) afin de ne pas dépendre du $PATH
- sfw.default -myversion pour obtenir la version de sfw, -v permettant d'obtenir celle de sfw & nft
- sfw.default en dehors des options -myv et -v toute autre commande débutant par un signe moins est refusée

Version 0.1.2 - 20230522
- sfw-common-sh: bug lors du remplacement des espaces par une virgule pour les sets ipv4 et ipv6 (getips). On remplace par un espace

Version 0.1.1 - 20230105
- sfw4: bug broadcast & multicast appliqués aux adresses local_net en lieu et place de 0.0.0.0 
- correction de la gestion du reset dans sfw-tool qui n'était pas complète

Version 0.1.0 - 20230130
  Augmentation de la sécurité, les machines sont moins vulnérables si les règles sont appliquées d'un seul tenant via nft -f <fichier>
  Cela permet également d'avoir un backup des dernières règles appliquées

  Principe: les règles à appliquer sont stockées dans des fichiers en fonction de leur teneur. L'ensemble est dans un répertoire défini par
  le nouveau parametre myoutput. Les fichiers de lock ont disparus, le paramètre resetRules s'il a la valeur "true" (valeur par défaut)
  recrée les tables, set et maps de base ainsi que les règles indépendantes des interfaces.
  NOTE: il est TRÈS FORTEMENT recommandé de conserver la valeur "true" pour ce paramètre

  Organisation du répertoire des règles:
  =====================================
  $myoutput contient 3 fichiers (nft.rules nft.rules-4 et nft.rules-6) et 3 sous répertoires (inet ip et ip6)
  nft.rules inclu les 3 sous répertoires, nft.rules-4 inclu les répertoires inet et ip et nft.rules-6 uniquement le répertoire ip6

  Dans chaque répertoire on trouve à minima 4 fichiers -5 si openIIF est utilisé- soit les fichiers:
  rules -règles de base des scripts-, rules.0 -règles de base utilisateurs, jamais modifié par les scripts-
  rules.lo -règles de l'interface local-, rules.openIIF -si le paramètre est utilisé- et rules.z -règles utilisateurs, jamais modifié par les scripts-
  Se trouveront ensuite des fichiers rules.NomDinterface pour chaque interface pour laquelle des règles existent.

  IMPORTANT: les fichiers sont lus par ordre alphabétique d'ou le nommage utilisé

  NOTE: les fichiers de type rules.[0|z] peuvent être suffixés comme par ex. rules.[00|zz] ou par un nom d'interface comme rules.[0|z]eth0
        Dans ce dernier cas les fichiers seront supprimés lors de la suppression des règles d'une interface (voir sfw-reset)

- les règles ne sont plus appliquées immédiatement mais inscrites dans les fichiers $myoutput/[inet|ip|ip6]/rules[.0|.IF name|.z]
  En fin de script sfw4 et sfw6, sont lues et appliquées les nouvelles directives
- sfw-flush est remplacé par sfw-tool
- sfw-tool possède 3 commandes:
  . exécuté sans paramètre, les règles sont réinitialisées puis les fichiers relus et appliqués. Si un second paramètre [ipv4|4] ou [ipv6|6] est passé,
  uniquement les règles concernant ce paramètre sont traitées. Le paramètre flush aura pour effet de supprimer les règles sans aucune relecture
  des règles existantes (équivalent à "nft flush ruleset")
  . avec le paramètre remove=IFACE ou IFACE est le nom d'une interface, les fichiers contenant les règles concernant cette interface sont supprimés
  *** Y COMPRIS celles créées par l'utilisateur ***  puis les règles rechargées. Si un second paramètre [ipv4|4] ou [ipv6|6] est passé,uniquement
  les règles concernant ce paramètre seront supprimées
  . avec le paramètre reset=IFACE ou IFACE est le nom d'une interface, les fichiers contenant les règles concernant cette interface sont supprimés
  *** SAUF celles créées par l'utilisateur *** puis les règles pour cette interface recréées et rechargées
- sfw voit une nouvelle commande reload qui exécute sfw-tool sans paramètre
- les fichiers sfw[4|6].local[.IFACE] existants ainsi que /etc/default/sfw sont à adapter au nouveau format
- correction de language du fichier README.md pour la version v20220909

Version 0.0.3 - 20230106
- sfw6: bug variable $myaddripv4to6 inexistante, $addripv4to6 est l'existante 

Version 20220909
- sfw-common création du set blocklist afin de pouvoir bloquer un réseau ou une IP tous ports et protocoles confondus
- sfw4 & sfw6 rajout dans nat prerouting saddr @blocklist drop
 
Version 20220831
- sfw6: les adresses $sitePrefix sont autorisées vers les ipv6 GUA et ULA si ces dernières existent
- sfw.default et sfw-common.sh: dans la liste des adresses IPv4 et IPv6 les espaces et ligne feed sont remplacés par des virgules
- sfw4: ct dnat accepte pour forward toutes interfaces confondues

Version 20210310
- sfw.default: on ne recherche les adresses IP uniquement lors de la mise en place des règles pare-feu
- sfw-flush  : possibilité de réinitialiser les règles pour ipv4 et/ou ipv6 en mode normal ou dur

Version 20210206

## Les fichiers
#

* sfw.default, fichier de configuration principale
* sfw-common, fonctions et procédures communes
* sfw[4|6] => règles appliquées pour ipv4 et/ou ipv6
* sfw[4|6].local => règle-s locale-s ipv4 et/ou ipv6 à appliquer en complément des règles de base
* sfw[4|6].local.NomDInterface => règle-s locale-s ipv4 et/ou ipv6 à appliquer en complément des règles de base, règles locales -s'il y en a- *UNIQUEMENT* pour l'interface suffixée
* sfw, dispatcher pour NetworkManager. De préférence, installer sfw dans le répertoire des scripts et créer un lien symbolique vers ce fichier dans etc.network.if-up.d
* sfw.service, configuration pour systemd (si utilisé)
* sfw-tool, remplace sfw-flush
* sfw-iproute, gestion de la table de routage iproute2

## Programmes complémentaires
#

1. grep
2. ls
3. bc
4. systemctl
5. sed
6. ip

Dépréciés
=========
* sfw-flush, permet de supprimer/réinialiser les règles, toutes, uniquement ipv4, uniquement ipv6
* sfw-accept, accepte tout le trafic ipv4/ipv6 pour une interface
* sfwd.service, configuration pour systemd
* sfwd, daemon de surveillance
* sfw-monitor, surveillance de sfwd
* udev
* incron
